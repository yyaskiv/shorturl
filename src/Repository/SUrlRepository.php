<?php

namespace App\Repository;

use App\Entity\SUrl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SUrl|null find($id, $lockMode = null, $lockVersion = null)
 * @method SUrl|null findOneBy(array $criteria, array $orderBy = null)
 * @method SUrl[]    findAll()
 * @method SUrl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SUrlRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SUrl::class);
    }

    public function findOneByUrl($value): ?SUrl
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.short_url = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
