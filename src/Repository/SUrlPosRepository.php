<?php

namespace App\Repository;

use App\Entity\SUrlPos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SUrlPos|null find($id, $lockMode = null, $lockVersion = null)
 * @method SUrlPos|null findOneBy(array $criteria, array $orderBy = null)
 * @method SUrlPos[]    findAll()
 * @method SUrlPos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SUrlPosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SUrlPos::class);
    }
}
