<?php

namespace App\Controller;

use App\Entity\SUrl;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\Request;

class SUrlController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('url/index.html.twig', [
            'form' => $this->makeForm()->createView()
        ]);
    }

    /**
     * @Route("/s/{url}", name="redir")
     */
    public function redir($url)
    {
        $em = $this->getDoctrine()->getManager();
        $sUrl = $em->getRepository(SUrl::class)->findOneByUrl($url);

        if (!$sUrl) {
            throw $this->createNotFoundException(
                'No url found'
            );
        }

        return $this->redirect($sUrl->getFullUrl());
    }

    /**
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request)
    {
        $sUrl = new SUrl();
        $form = $this->makeForm($sUrl);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sUrl);
            $em->flush();
            return $this->render('url/created.html.twig', [
                'url' => $sUrl->getShortUrl()
            ]);
        }
        return $this->redirectToRoute('index');
    }

    private function makeForm($sUrl = null) {
        if ($sUrl == null) {
            $sUrl = new SUrl();
        }
        return $this->createFormBuilder($sUrl)
            ->setAction($this->generateUrl('create'))
            ->setMethod('POST')
            ->add('full_url', UrlType::class)
            ->add('save', SubmitType::class)
            ->getForm();
    }
}
