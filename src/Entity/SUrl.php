<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SUrlRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SUrl
{
    private $chars = ['a', 'b', 'c', 'd', 'e'];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $short_url;

    /**
     * @ORM\Column(type="text")
     */
    private $full_url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullUrl(): ?string
    {
        return $this->full_url;
    }

    public function setFullUrl(string $full_url): self
    {
        $this->full_url = $full_url;

        return $this;
    }

    public function getShortUrl(): ?string
    {
        return $this->short_url;
    }

    public function setShortUrl(string $short_url): self
    {
        $this->short_url = $short_url;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setShortUrlValue(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $maxItem = count( $this->chars ) - 1;
        $items = $em->getRepository(SUrlPos::class)->findAll();
        $reversedItems = array_reverse($items);

        // try inrease some value, start from end
        $increased = 0;
        foreach($reversedItems as $item) {
            if ($item->getCPos() < $maxItem) {
                $newValue = $item->getCPos() + 1;
                $item->setCPos($newValue);
                $increased = $item->getId();
                break;
            }
        }
        if ($increased > 0) {
            foreach($items as $item) {
                if ($item->getId() > $increased) {
                    $item->setCPos(0);
                }
            }
            $em->flush();
        }

        // step 2, add one more item, set all to 0 position
        if ($increased == 0) {
            foreach($items as $item) {
                $item->setCPos(0);
            }

            $newOne = new SUrlPos();
            $newOne->setCPos(0);

            $em->persist($newOne);
            $em->flush();

            $items = $em->getRepository(SUrlPos::class)->findAll();
        }

        $val = "";
        foreach($items as $item) {
            $val .= $this->chars[ $item->getCPos() ];
        }

        $this->short_url = $val;
    }
}
