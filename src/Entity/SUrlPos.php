<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SUrlPosRepository")
 */
class SUrlPos
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $c_pos;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCPos(): ?int
    {
        return $this->c_pos;
    }

    public function setCPos(int $c_pos): self
    {
        $this->c_pos = $c_pos;

        return $this;
    }
}
